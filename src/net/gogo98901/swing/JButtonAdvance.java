package net.gogo98901.swing;

import java.awt.event.ActionEvent;
import java.util.concurrent.Semaphore;

import javax.swing.JButton;

import net.gogo98901.log.GOLog;

public class JButtonAdvance extends JButton {
	private static final long serialVersionUID = 1L;
	private Semaphore sem = new Semaphore(1);

	public JButtonAdvance(String arg0) {
		super(arg0);
	}

	public void actionPerformed(ActionEvent event) {
		sem.release();
	}

	public void waitForPress() throws InterruptedException {
		sem.acquire();
		GOLog.info("JButton [" + getText() + "] is waiting for press");
		sem.acquire();
	}
}
