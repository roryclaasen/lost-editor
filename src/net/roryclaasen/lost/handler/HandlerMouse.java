package net.roryclaasen.lost.handler;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import net.roryclaasen.lost.GameThread;

public class HandlerMouse implements MouseListener, MouseMotionListener, MouseWheelListener {

	public static final int BTN_LEFT = 1;
	public static final int BTN_WHEEL = 2;
	public static final int BTN_RIGHT = 3;
	public static final int BTN_MOUSE4 = 4;
	public static final int BTN_MOUSE5 = 5;
	public static final int BTN_MOUSE6 = 6;
	public static final int BTN_MOUSE7 = 7;
	public static final int BTN_MOUSE8 = 8;

	@SuppressWarnings("unused")
	private GameThread game;
	private int x, y;
	private int button = -1;
	private int wheel;

	public HandlerMouse(GameThread game) {
		this.game = game;
	}

	@Override
	public void mouseClicked(MouseEvent event) {

	}

	@Override
	public void mouseEntered(MouseEvent event) {

	}

	@Override
	public void mouseExited(MouseEvent event) {

	}

	@Override
	public void mousePressed(MouseEvent event) {
		button = event.getButton();
	}

	@Override
	public void mouseReleased(MouseEvent event) {
		button = -1;
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent event) {
		@SuppressWarnings("unused")
		int steps = event.getWheelRotation();
		//if (steps < 0) game.canvas.getUserInterface().getInv().up(-steps);
		//else game.canvas.getUserInterface().getInv().down(steps);
	}

	@Override
	public void mouseDragged(MouseEvent event) {
		this.x = event.getX();
		this.y = event.getY();
	}

	@Override
	public void mouseMoved(MouseEvent event) {
		this.x = event.getX();
		this.y = event.getY();
	}

	public void update() {
		
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getButton() {
		return button;
	}

	public int getWheel() {
		return wheel;
	}
}
