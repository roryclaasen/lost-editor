package net.roryclaasen.lost.handler;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.window.DisplayWindow;

public class Handler {
	private GameThread game;
	public HandlerWindow window;
	public HandlerKeyboard keyboard;
	public HandlerMouse mouse;

	public Handler(GameThread game) {
		this.game = game;
		window = new HandlerWindow(game);
		keyboard = new HandlerKeyboard();
		mouse = new HandlerMouse(game);
	}

	public void addAll() {
		DisplayWindow.getFrame().addWindowListener(window);
		DisplayWindow.getFrame().addComponentListener(window);
		DisplayWindow.getFrame().addWindowFocusListener(window);

		game.canvas.addKeyListener(keyboard);
		game.canvas.addMouseListener(mouse);
		game.canvas.addMouseMotionListener(mouse);
		game.canvas.addMouseWheelListener(mouse);

		game.canvas.requestFocus();

		Log.info("Handlers... ADDED");
	}
}
