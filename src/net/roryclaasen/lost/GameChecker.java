package net.roryclaasen.lost;

import net.gogo98901.log.Log;

public class GameChecker implements Runnable {
	private Thread _thread;

	private GameThread game;

	private boolean running = false;

	private boolean frozen = false;

	public GameChecker(GameThread game) {
		this.game = game;
	}

	public void start() {
		Log.info("Starting Game Checker!");
		running = true;
		_thread = new Thread(this, "_checker");
		_thread.start();
	}

	public void stop() {
		Log.info("Stopping Game Checker!");
		running = false;
		try {
			_thread.join();
		} catch (InterruptedException e) {
			Log.stackTrace(e);
		}
	}

	@Override
	public void run() {
		int oldTime = -1;
		while (running) {
			if (GameThread.hasStarted()) {
				long mills = System.currentTimeMillis();
				if (oldTime == game.time()) {
					frozen = true;
				} else frozen = false;
				oldTime = game.time();

				if (frozen) Log.warn("Has the Game frozen?");
				try {
					Thread.sleep(1000 - mills % 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		stop();
	}
}
