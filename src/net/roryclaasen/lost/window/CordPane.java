package net.roryclaasen.lost.window;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.PlainDocument;

import net.gogo98901.swing.IntFilter;

public class CordPane extends JPanel {
	private static final long serialVersionUID = 1L;
	private JLabel xText, yText;

	private JTextField xCord, yCord;

	public static int levelWidth, levelHeight;

	private Point last;

	public CordPane() {
		setLayout(null);
	}

	public void init() {
		xText = new JLabel("X Coordinate");
		xText.setBounds(0, 0, getWidth(), 20);
		xText.setHorizontalAlignment(SwingConstants.CENTER);
		add(xText);

		xCord = new JTextField(3);
		((PlainDocument) xCord.getDocument()).setDocumentFilter(new IntFilter());
		xCord.setBounds(getWidth() / 2 - 25, xText.getY() + xText.getHeight() + 5, 50, 20);
		xCord.setText("0");
		add(xCord);

		JButton xDown = new JButton("Decrease");
		xDown.setBounds(xCord.getX() - 10 - 100, xCord.getY(), 100, 20);
		xDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int orginal = Integer.parseInt(xCord.getText());
				int output = orginal - 1;
				if (output >= 0) xCord.setText("" + output);
			}
		});
		add(xDown);
		JButton xUp = new JButton("Increase");
		xUp.setBounds(xCord.getX() + xCord.getWidth() + 10, xCord.getY(), 100, 20);
		xUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int orginal = Integer.parseInt(xCord.getText());
				int output = orginal + 1;
				if (output < levelWidth) xCord.setText("" + output);
			}
		});
		add(xUp);

		yText = new JLabel("Y Coordinate");
		yText.setBounds(0, xText.getHeight() + 30, getWidth(), 20);
		yText.setHorizontalAlignment(SwingConstants.CENTER);
		add(yText);

		yCord = new JTextField(3);
		((PlainDocument) yCord.getDocument()).setDocumentFilter(new IntFilter());
		yCord.setBounds(getWidth() / 2 - 25, yText.getY() + yText.getHeight() + 5, 50, 20);
		yCord.setText("0");
		add(yCord);

		JButton yDown = new JButton("Decrease");
		yDown.setBounds(yCord.getX() - 10 - 100, yCord.getY(), 100, 20);
		yDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int orginal = Integer.parseInt(yCord.getText());
				int output = orginal - 1;
				if (output >= 0) yCord.setText("" + output);
			}
		});
		add(yDown);
		JButton yUp = new JButton("Increase");
		yUp.setBounds(yCord.getX() + yCord.getWidth() + 10, yCord.getY(), 100, 20);
		yUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int orginal = Integer.parseInt(yCord.getText());
				int output = orginal + 1;
				if (output < levelWidth) yCord.setText("" + output);
			}
		});
		add(yUp);
	}

	public Point getCords() {
		try {
			int x = Integer.parseInt(xCord.getText());
			int y = Integer.parseInt(yCord.getText());
			last = new Point(x, y);
		} catch (Exception e) {

		}
		return last;
	}

	public void setCords(int x, int y) {
		xCord.setText("" + x);
		yCord.setText("" + y);
	}

}
