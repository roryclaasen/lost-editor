package net.roryclaasen.lost.window;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.toolbox.Strings;

public class Selector {
	private SpriteSheet sheet;
	private JFrame frame;

	private List<JButton> sprites = new ArrayList<JButton>();

	private final Options options;

	private int id;

	private JButton done, close;

	private Selector sel;

	public Selector(SpriteSheet sheet, final Options options) {
		this.sheet = sheet;
		this.options = options;
		this.sel = this;
		initFrame();
		for (int i = 0; i < sheet.size(); i++) {
			final int id = i;
			JButton sprite = new JButton();
			sprite.setIcon(new ImageIcon(sheet.get(i).getImage()));
			sprite.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					setId(id);
				}
			});
			sprites.add(sprite);
		}
		frame.setSize(new Dimension((int) Math.sqrt(sheet.size()) * 32 + 6, (int) Math.sqrt(sheet.size()) * 32 + 60));
		for (int y = 0; y < Math.sqrt(sheet.size()); y++) {
			for (int x = 0; x < Math.sqrt(sheet.size()); x++) {
				if (x + y * Math.sqrt(sheet.size()) < sheet.size()) {
					JButton current = sprites.get((int) (x + y * Math.sqrt(sheet.size())));
					current.setBounds(32 * x, 32 * y, 32, 32);
					frame.add(current);
				}
			}
		}
		done = new JButton("Accecpt");
		done.setFont(done.getFont().deriveFont(Font.BOLD));
		done.setBounds(10, sprites.get(sprites.size() - 1).getY() + 35, frame.getWidth() / 2 - 20, 20);
		done.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				options.setId(sel, id);
				hide();
			}
		});
		frame.add(done);
		close = new JButton("Cancel");
		close.setFont(done.getFont());
		close.setBounds(done.getX() + done.getWidth() + 10, done.getY(), done.getWidth(), done.getHeight());
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				hide();
			}
		});
		frame.add(close);

		frame.setLocationRelativeTo(options);
	}

	private void initFrame() {
		frame = new JFrame();
		frame.setTitle(Strings._title + " - " + sheet.getName());
		frame.setSize(600, 600);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(null);
	}

	public void show() {
		frame.setLocationRelativeTo(options);
		frame.setVisible(true);
		frame.revalidate();
	}

	public void hide() {
		frame.setVisible(false);
	}

	private void setId(int id) {
		this.id = id;
	}

	public BufferedImage getImage() {
		return sheet.get(id).getImage();
	}
}
