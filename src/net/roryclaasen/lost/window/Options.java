package net.roryclaasen.lost.window;

import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.text.PlainDocument;

import net.gogo98901.log.Log;
import net.gogo98901.popup.Dialogs;
import net.gogo98901.swing.IntFilter;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.level.LItem;
import net.roryclaasen.lost.level.LItemDrop;
import net.roryclaasen.lost.level.LMob;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.toolbox.TileCords;

public class Options extends JPanel {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private GameCanvas canvas;
	private Level level;

	private JButton save, open, cPlayer;
	private JButton item, mob, tilecord;

	private ScrollPane addOptionsScroll;
	private JPanel itemPanel, mobPanel, tilePanel;

	private int currentItemId = 0;
	private int currentMobId = 0;

	private List<JLabel> icons = new ArrayList<JLabel>();

	private Selector selectorItem;
	private Selector selectorMob;

	private IDpage pageItems;

	private CordPane cords;

	public Options(CordPane cords) {
		this.cords = cords;
		setLayout(null);
	}

	public void init() {
		selectorItem = new Selector(SpriteSheet.items, this);
		selectorMob = new Selector(SpriteSheet.mobs, this);

		pageItems = new IDpage(SpriteSheet.items, this);

		open = new JButton("Open");
		open.setBounds(10, 10, 100, 20);
		open.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				level.loadLevel();
			}
		});
		add(open);
		save = new JButton("Save");
		save.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				level.save();
			}
		});
		save.setBounds(120, 10, 100, 20);
		add(save);
		cPlayer = new JButton("Center On Player");
		cPlayer.setBounds(230, 10, 150, 20);
		cPlayer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				int x = level.getPlayer().getPosition().getX();
				int y = level.getPlayer().getPosition().getY();
				cords.setCords(x, y);
				Log.info("World centered on player [x=" + x + ", y=" + y + "]");
			}
		});
		add(cPlayer);

		addOptionsScroll = new ScrollPane();
		addOptionsScroll.setBounds(10, 80, 375, 500);
		// addOptionsScroll.setBackground(Color.WHITE);
		add(addOptionsScroll);
		{
			itemPanel = new JPanel();
			itemPanel.setLayout(null);
			// itemPanel.setBackground(Color.WHITE);
			JLabel title = new JLabel("Add Item");
			title.setBounds(0, 0, addOptionsScroll.getWidth(), 20);
			title.setHorizontalAlignment(SwingConstants.CENTER);
			title.setFont(getFont().deriveFont(18F));
			itemPanel.add(title);

			JButton open = new JButton("Select");
			open.setBounds(10, 40, 100, 20);
			open.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					selectorItem.show();
				}
			});
			itemPanel.add(open);

			JLabel icon = new JLabel();
			icon.setIcon(new ImageIcon(SpriteSheet.items.get(currentItemId).getImage()));
			icon.setBounds(115, 35, 32, 32);
			icons.add(icon);
			itemPanel.add(icon);

			JLabel lbQuant = new JLabel("Quantity");
			lbQuant.setVerticalAlignment(SwingConstants.CENTER);
			lbQuant.setHorizontalAlignment(SwingConstants.CENTER);
			lbQuant.setBounds(open.getX(), icon.getY() + icon.getHeight() + 5, open.getWidth(), 20);
			itemPanel.add(lbQuant);

			final JTextField quant = new JTextField(2);
			((PlainDocument) quant.getDocument()).setDocumentFilter(new IntFilter());
			quant.setBounds(icon.getX(), icon.getY() + icon.getHeight() + 5, 50, 20);
			quant.setText("1");
			itemPanel.add(quant);

			JButton add = new JButton("Add to level");
			add.setBounds(10, 300, 100, 20);
			add.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					LItemDrop item = new LItemDrop(currentItemId, Integer.parseInt(quant.getText()), new TileCords(cords.getCords().x, cords.getCords().y));
					level.add(item);
				}
			});
			itemPanel.add(add);

			item = new JButton("Add item");
			item.setBounds(10, 40, 100, 20);
			item.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					// RESET
					quant.setText("1");
					currentItemId = 0;

					addOptionsScroll.removeAll();
					addOptionsScroll.add(itemPanel);
					addOptionsScroll.revalidate();
				}
			});
			add(item);
		}
		{
			mobPanel = new JPanel();
			mobPanel.setLayout(null);
			// mobPanel.setBackground(Color.WHITE);
			JLabel title = new JLabel("Add Mob");
			title.setBounds(0, 0, addOptionsScroll.getWidth(), 20);
			title.setHorizontalAlignment(SwingConstants.CENTER);
			title.setFont(getFont().deriveFont(18F));
			mobPanel.add(title);

			JButton open = new JButton("Select");
			open.setBounds(10, 40, 100, 20);
			open.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					selectorMob.show();
				}
			});
			mobPanel.add(open);

			JLabel icon = new JLabel();
			icon.setIcon(new ImageIcon(SpriteSheet.mobs.get(currentMobId).getImage()));
			icon.setBounds(115, 35, 32, 32);
			icons.add(icon);
			mobPanel.add(icon);

			final JLabel currentItem = new JLabel();
			currentItem.setBounds(10, 125, 40, 40);
			currentItem.setBorder(new EtchedBorder());
			mobPanel.add(currentItem);

			final JComboBox<String> itemList = new JComboBox<String>();
			itemList.addPropertyChangeListener(new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent arg0) {
					if (itemList.getItemCount() > 0) {
						currentItem.setIcon(pageItems.getImageFromId(Integer.parseInt(itemList.getSelectedItem().toString().split(",")[0].replaceFirst("id=", ""))));
					}
				}
			});
			itemList.setBounds(currentItem.getX() + currentItem.getWidth() + 5, 135, 100, 20);
			mobPanel.add(itemList);

			JButton addToList = new JButton("Add item");
			addToList.setBounds(itemList.getX() + itemList.getWidth() + 2, itemList.getY(), itemList.getWidth(), itemList.getHeight());
			addToList.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					String stringID = JOptionPane.showInputDialog("Enter the ID");
					if (!stringID.isEmpty()) {
						int id = -1;
						try {
							id = Integer.parseInt(stringID);
						} catch (Exception e) {
							Dialogs.warning("You can only enter a number", "Enter the ID");
						}
						if (id != -1) {
							String stringQ = JOptionPane.showInputDialog("Enter the quantity of item [" + id + "]");
							if (!stringQ.isEmpty()) {
								int q = -1;
								try {
									q = Integer.parseInt(stringQ);
								} catch (Exception e) {
									Dialogs.warning("You can only enter a number", "Enter the ID");
								}
								if (q != -1) {
									currentItem.setIcon(pageItems.getImageFromId(id));
									currentItem.setToolTipText("id=" + id + ", no=" + q);
									itemList.addItem(currentItem.getToolTipText());
								}
							}
						}
					}
				}
			});
			mobPanel.add(addToList);

			JButton removeFromList = new JButton("Remove item");
			removeFromList.setBounds(addToList.getX() + addToList.getWidth() + 5, addToList.getY(), addToList.getWidth(), addToList.getHeight());
			removeFromList.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					if (itemList.getItemCount() > 0) {
						itemList.removeItemAt(itemList.getSelectedIndex());
					}
					if (itemList.getItemCount() > 0) {
						currentItem.setIcon(pageItems.getImageFromId(Integer.parseInt(itemList.getSelectedItem().toString().split(",")[0].replaceFirst("id=", ""))));
					} else currentItem.setIcon(null);
				}
			});
			mobPanel.add(removeFromList);

			JButton showItems = new JButton("Show Item IDs");
			showItems.setBounds(currentItem.getX() - 1, currentItem.getY() + 45, 150, 20);
			showItems.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent arg0) {
					pageItems.show();
				}
			});
			mobPanel.add(showItems);

			JButton add = new JButton("Add to level");
			add.setBounds(10, 300, 100, 20);
			add.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent arg0) {
					List<LItem> loot = new ArrayList<LItem>();
					for (int i = 0; i < itemList.getItemCount(); i++) {
						String[] parts = itemList.getItemAt(i).split(", ");
						LItem item = new LItem(Integer.parseInt(parts[0].replace("id=", "")), Integer.parseInt(parts[1].replace("no=", "")));
						loot.add(item);
					}
					LMob mob = new LMob(currentMobId, cords.getCords().x, cords.getCords().y, true, 1, loot);
					level.add(mob);
				}
			});
			mobPanel.add(add);

			mob = new JButton("Add mob");
			mob.setBounds(120, 40, 100, 20);
			mob.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					addOptionsScroll.removeAll();
					addOptionsScroll.add(mobPanel);
					addOptionsScroll.revalidate();
				}
			});
			add(mob);
		}
		{
			tilePanel = new JPanel();
			tilePanel.setLayout(null);
			// itemPanel.setBackground(Color.WHITE);
			JLabel title = new JLabel("Tile ");
			title.setHorizontalAlignment(SwingConstants.CENTER);
			title.setBounds(0, 0, addOptionsScroll.getWidth(), 20);
			title.setFont(getFont().deriveFont(18F));
			tilePanel.add(title);
			
			ActionListener updateAll = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					
				}
			};

			JSeparator sep = new JSeparator();
			sep.setBounds(10, title.getY() + 20, addOptionsScroll.getWidth() - 20, 20);
			tilePanel.add(sep);

			JLabel lbItems = new JLabel("Items");
			lbItems.setBounds(0, title.getY() + 100, addOptionsScroll.getWidth(), 20);
			lbItems.setHorizontalAlignment(SwingConstants.CENTER);
			lbItems.setFont(getFont().deriveFont(18F));
			tilePanel.add(lbItems);

			final JLabel currentItem = new JLabel();
			currentItem.setBounds(10, lbItems.getY() + lbItems.getHeight() + 20, 40, 40);
			currentItem.setBorder(new EtchedBorder());
			tilePanel.add(currentItem);

			final JComboBox<String> itemList = new JComboBox<String>();
			itemList.addPropertyChangeListener(new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent arg0) {
					if (itemList.getItemCount() > 0) {
						currentItem.setIcon(pageItems.getImageFromId(Integer.parseInt(itemList.getSelectedItem().toString().split(",")[0].replaceFirst("id=", ""))));
					}
				}
			});
			itemList.setBounds(currentItem.getX() + currentItem.getWidth() + 5, currentItem.getY(), 200, 20);
			tilePanel.add(itemList);

			JButton removeItem = new JButton("Remove Selected item");
			removeItem.setBounds(itemList.getX(), itemList.getY() + itemList.getHeight() + 5, itemList.getWidth(), 20);
			tilePanel.add(removeItem);

			JLabel lbMobs = new JLabel("Mobs");
			lbMobs.setBounds(0, addOptionsScroll.getHeight() / 2, addOptionsScroll.getWidth(), 20);
			lbMobs.setHorizontalAlignment(SwingConstants.CENTER);
			lbMobs.setFont(getFont().deriveFont(18F));
			tilePanel.add(lbMobs);

			final JLabel currentMob = new JLabel();
			currentMob.setBounds(10, lbMobs.getY() + lbMobs.getHeight() + 20, 40, 40);
			currentMob.setBorder(new EtchedBorder());
			tilePanel.add(currentMob);

			final JComboBox<String> mobList = new JComboBox<String>();
			mobList.addPropertyChangeListener(new PropertyChangeListener() {

				@Override
				public void propertyChange(PropertyChangeEvent arg0) {
					if (mobList.getItemCount() > 0) {
						currentItem.setIcon(pageItems.getImageFromId(Integer.parseInt(mobList.getSelectedItem().toString().split(",")[0].replaceFirst("id=", ""))));
					}
				}
			});
			mobList.setBounds(currentItem.getX() + currentItem.getWidth() + 5, currentMob.getY(), 200, 20);
			tilePanel.add(mobList);

			JButton removeMob = new JButton("Remove Selected mob");
			removeMob.setBounds(mobList.getX(), mobList.getY() + mobList.getHeight() + 5, mobList.getWidth(), 20);

			tilePanel.add(removeMob);
			JButton updateList = new JButton("Update");
			updateList.setBounds(addOptionsScroll.getWidth() - 110, addOptionsScroll.getHeight() - 135, 100, 20);
			updateList.addActionListener(updateAll);
			tilePanel.add(updateList);
			
			tilecord = new JButton("Select Tile");
			tilecord.setBounds(230, 40, 100, 20);
			tilecord.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					addOptionsScroll.removeAll();
					addOptionsScroll.add(tilePanel);
					addOptionsScroll.revalidate();
				}
			});
			add(tilecord);
		}
	}

	public void setCanvas(GameCanvas canvas) {
		this.canvas = canvas;
		this.level = canvas.getLevel();
	}

	public void setId(Selector sel, int id) {
		if (sel.equals(selectorItem)) {
			currentItemId = id;
			icons.get(0).setIcon(new ImageIcon(sel.getImage()));
			icons.get(0).revalidate();
		} else if (sel.equals(selectorMob)) {
			currentMobId = id;
			icons.get(1).setIcon(new ImageIcon(sel.getImage()));
			icons.get(1).revalidate();
		}
	}
}
