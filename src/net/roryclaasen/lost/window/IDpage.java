package net.roryclaasen.lost.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.toolbox.Strings;

public class IDpage {
	private SpriteSheet sheet;
	private JFrame frame;
	private Options options;

	private List<JLabel> sprites = new ArrayList<JLabel>();
	private JButton close;

	public IDpage(SpriteSheet sheet, Options options) {
		this.sheet = sheet;
		this.options = options;
		initFrame();
		JPanel panel = new JPanel();
		panel.setLayout(null);
		for (int i = 0; i < sheet.size(); i++) {
			final int id = i;
			JLabel lbl = new JLabel(id + "");
			lbl.setIcon(new ImageIcon(sheet.get(i).getImage()));
			if (i < sheet.size() / 3) lbl.setBounds(10, 10 + (35 * i), 75, 32);
			else if (i < (sheet.size() / 3) * 2) lbl.setBounds(frame.getWidth() / 3 - 10, 10 + (35 * (i - sheet.size() / 3)), 75, 32);
			else lbl.setBounds((frame.getWidth() / 3) * 2 - 10, 10 + (35 * (i - (sheet.size() / 3) * 2)), 75, 32);

			panel.add(lbl);
			sprites.add(lbl);
		}
		JScrollPane pane = new JScrollPane(panel);
		pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		panel.setPreferredSize(new Dimension(frame.getWidth() - 20, (sprites.size() / 3 + 2) * 35 + 10));
		frame.add(pane, BorderLayout.CENTER);
		pane.revalidate();
		close = new JButton("Close");
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		frame.add(close, BorderLayout.SOUTH);

		frame.setLocationRelativeTo(options);
	}

	private void initFrame() {
		frame = new JFrame();
		frame.setTitle(Strings._title + " - " + sheet.getName());
		frame.setSize(300, 600);
		frame.setResizable(false);
		frame.setAlwaysOnTop(true);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setLocationRelativeTo(null);
	}

	public void show() {
		frame.setLocationRelativeTo(options);
		frame.setVisible(true);
		frame.revalidate();
	}

	public void hide() {
		frame.setVisible(false);
	}

	public JLabel getFromId(int id) {
		return sprites.get(id);
	}

	public Icon getImageFromId(int id) {
		return sprites.get(id).getIcon();
	}
}
