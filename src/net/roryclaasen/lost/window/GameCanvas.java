package net.roryclaasen.lost.window;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferStrategy;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.level.Level;
import net.roryclaasen.lost.toolbox.Colors;

public class GameCanvas extends Canvas {

	private static final long serialVersionUID = 1L;

	private GameThread game;
	private Level level;

	private int width, height, fadeDir = 1, opacity;

	private Point renderPoint = new Point(0, 0);
	private Point selectPoint = new Point(300, 300);

	public GameCanvas(GameThread game, int width, int height) {
		this.game = game;
		this.width = width;
		this.height = height;
		setBackground(new Color(25, 25, 25));
		selectPoint = new Point(width / 2 - 16, width / 2 - 16);
	}

	public void init() {
		try {
			level = new Level();
		} catch (Exception e) {
			Log.severe("Canvas+ Initialization... FAILED");
			Log.stackTrace(java.util.logging.Level.SEVERE, e);
			return;
		}
		Log.info("Canvas+ Initialization... OKAY");
	}

	public void start() {
		level.loadLevel();
	}

	public void render() {
		try {
			BufferStrategy bs = getBufferStrategy();
			if (bs == null) {
				createBufferStrategy(3);
				return;
			}
			Graphics g = bs.getDrawGraphics();
			g.clearRect(0, 0, width, height);

			level.render(g, selectPoint, renderPoint);
			g.setColor(Colors.setOpacity(Color.WHITE, opacity));
			g.fillRect(selectPoint.x, selectPoint.y, 32, 32);
			g.setColor(Color.CYAN);
			g.drawRect(selectPoint.x, selectPoint.y, 32, 32);
			g.dispose();
			bs.show();
		} catch (Exception e) {
			Log.warn("Bad render run... " + e);
			Log.stackTrace(e);
		}
	}

	public void update() {
		game.handler.keyboard.update();
		game.handler.mouse.update();
		renderPoint = DisplayWindow.getScrollPoint();

		if (fadeDir == -1) {
			opacity -= 3;
			if (opacity < 0) {
				opacity = 0;
				fadeDir = 1;
			}
		} else if (fadeDir == 1) {
			opacity += 3;
			if (opacity > 75) {
				opacity = 75;
				fadeDir = -1;
			}
		}
	}

	public Level getLevel() {
		return level;
	}

	public Point getRenderPoint() {
		return renderPoint;
	}

	public Point getSelectedPoint() {
		return new Point(selectPoint.x / 32, selectPoint.y / 32);
	}
}
