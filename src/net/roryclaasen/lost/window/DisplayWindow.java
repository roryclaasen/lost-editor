package net.roryclaasen.lost.window;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JFrame;

import net.gogo98901.log.Log;
import net.gogo98901.util.Loader;
import net.roryclaasen.lost.toolbox.Strings;

public class DisplayWindow {
	private static JFrame frame;
	private static int canvasWidth, canvasHeight;
	private static int width, height;

	@SuppressWarnings("unused")
	private static String _title;

	private static Options options;
	private static CordPane cords;

	public DisplayWindow() {
		canvasWidth = 600;
		width = 1000;
		canvasHeight = height = 600;
	}

	public void init() {
		frame = new JFrame();
		initFrame();
	}

	private void initFrame() {
		frame.setPreferredSize(new Dimension(width + 6, height + 29));
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setTitle(_title = Strings._title + " by " + Strings._author);
		frame.setResizable(false);
		try {
			BufferedImage icon = Loader.loadBufferedImagefromJar("assets/textures/mobs.png");
			icon = icon.getSubimage(0, 0, 32, 32);
			frame.setIconImage(icon);
		} catch (IOException e) {
		}
	}

	public void addComponents(GameCanvas canvas) {
		Container pane = frame.getContentPane();
		pane.setLayout(null);
		canvas.setBounds(0, 0, canvasWidth, canvasHeight);
		pane.add(canvas);
		Log.info("Frame added Canvas");
		
		cords = new CordPane();
		cords.setBounds(canvasWidth + 10, height - 125 + 10, width - canvasWidth - 20, 125 - 20);
		cords.init();
		pane.add(cords);
		Log.info("Frame cords panel");
		
		options = new Options(cords);
		options.setBounds(canvasWidth, 0, width - canvasWidth, height - 125);
		options.setCanvas(canvas);
		options.init();
		pane.add(options);
		Log.info("Frame options panel");

	}

	public static void setVisible(Boolean arg0) {
		frame.setVisible(arg0);
	}

	public static JFrame getFrame() {
		return frame;
	}

	public static final int getWidth() {
		return width;
	}

	public static final int getHeight() {
		return height;
	}

	public static final int getCanvasWidth() {
		return canvasWidth;
	}

	public static final int getCanvasHeight() {
		return canvasHeight;
	}

	public static Point getScrollPoint() {
		return cords.getCords();
	}

	public static void updateTitle(int frames, int updates) {
		//frame.setTitle(_title + " | " + frames + "fps");
	}
}
