package net.roryclaasen.lost.graphics.sprite;

import java.awt.image.BufferedImage;

public class Sprite {
	private BufferedImage image;
	private int id;
	private int width, height;

	public Sprite(BufferedImage image, int id) {
		this.image = image;
		this.id = id;
		if (image != null) {
			width = image.getWidth();
			height = image.getHeight();
		}
	}

	public BufferedImage getImage() {
		return image;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getID() {
		return id;
	}
}
