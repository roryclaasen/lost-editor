package net.roryclaasen.lost.graphics.sprite;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.gogo98901.log.Log;
import net.gogo98901.util.Loader;

public class SpriteSheet {
	public static final SpriteSheet sheet1 = new SpriteSheet("Sheet.png");
	public static final SpriteSheet items = new SpriteSheet("item.png");
	public static final SpriteSheet mobs = new SpriteSheet("mobs.png");

	private BufferedImage sheet;
	private String name;

	private List<Sprite> sprites = new ArrayList<Sprite>();

	public SpriteSheet(String name) {
		this.name = name;
		load();
	}

	private void load() {
		try {
			sheet = Loader.loadImagefromJar("assets/textures/" + name);
			for (int y = 0; y < sheet.getHeight() / 32; y++) {
				for (int x = 0; x < sheet.getWidth() / 32; x++) {
					sprites.add(new Sprite(sheet.getSubimage(x * 32, y * 32, 32, 32), y * sheet.getHeight() + x));
				}
			}
			Log.info("Loaded Spritesheet [" + name + "]");
		} catch (IOException e) {
			Log.stackTrace(e);
		}
	}

	public Sprite get(int id) {
		return sprites.get(id);
	}

	public BufferedImage getSheet() {
		return sheet;
	}

	public String getName() {
		return name;
	}

	public int size() {
		return sprites.size();
	}
}
