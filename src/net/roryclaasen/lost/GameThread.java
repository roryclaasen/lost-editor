package net.roryclaasen.lost;

import java.awt.event.WindowEvent;
import java.util.logging.Level;

import net.gogo98901.log.Log;
import net.roryclaasen.lost.handler.Handler;
import net.roryclaasen.lost.window.DisplayWindow;
import net.roryclaasen.lost.window.GameCanvas;

public class GameThread implements Runnable {
	private Thread _thread;

	private boolean running = false;
	private static boolean started = false;

	private DisplayWindow window;
	public GameThread game;
	public GameCanvas canvas;
	private GameChecker checker;

	public Handler handler;

	public int currentFrames, currentUpdates;
	private int time;

	public GameThread(DisplayWindow window) {
		game = this;
		this.window = window;
	}

	public void init() {
		try {
			canvas = new GameCanvas(game, DisplayWindow.getCanvasWidth(), DisplayWindow.getCanvasHeight());
			canvas.init();
			window.addComponents(canvas);

			checker = new GameChecker(game);

			handler = new Handler(game);
			handler.addAll();
		} catch (Exception e) {
			Log.severe("Game Initialization... FAILED");
			Log.stackTrace(Level.SEVERE, e);
			return;
		}
		Log.info("Game Initialization... OKAY");
	}

	public void start() {
		Log.info("Starting Game Thread!");
		_thread = new Thread(this, "_game");
		_thread.start();
		checker.start();
		running = true;
	}

	public void stop() {
		Log.info("Stopping Game Thread!");
		running = false;
		checker.stop();
		try {
			_thread.join();
		} catch (InterruptedException e) {
			Log.stackTrace(e);
		}
	}

	public void close() {
		DisplayWindow.getFrame().dispatchEvent(new WindowEvent(DisplayWindow.getFrame(), WindowEvent.WINDOW_CLOSING));
	}

	@Override
	public void run() {
		try {
			canvas.start();
		} catch (Exception e) {
			Log.severe("Game Peripherals... FAILED");
			Log.stackTrace(Level.SEVERE, e);
			close();
		}
		Log.info("Game Peripherals... OKAY");
		Log.info("Game has started!");
		started = true;

		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1000000000.0 / 60.0;
		double delta = 0.0;
		int updates = 0;
		int frames = 0;
		canvas.requestFocus();
		while (running) {
			time++;
			if (time % 1200 == 0) time = 0;
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				canvas.update();
				updates++;
				delta--;
			}
			canvas.render();
			frames++;

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				currentFrames = frames;
				currentUpdates = updates;
				DisplayWindow.updateTitle(frames, updates);
				updates = 0;
				frames = 0;
			}
		}
		Log.info("Game has stopped!");
		stop();
	}

	public int time() {
		return time;
	}

	public int getCurrentFrames() {
		return currentFrames;
	}

	public int getCurrentUpdates() {
		return currentUpdates;
	}

	public static boolean hasStarted() {
		return started;
	}
}
