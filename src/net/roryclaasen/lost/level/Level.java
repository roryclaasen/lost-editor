package net.roryclaasen.lost.level;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;

import net.gogo98901.log.Log;
import net.gogo98901.popup.Dialogs;
import net.gogo98901.reader.StaticReader;
import net.gogo98901.util.Loader;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.Strings;
import net.roryclaasen.lost.toolbox.TileCords;
import net.roryclaasen.lost.window.CordPane;
import net.roryclaasen.lost.window.DisplayWindow;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Level {

	private String levelName;
	private boolean loaded = false;
	private JSONParser parser = new JSONParser();
	private JFileChooser fileChooser = new JFileChooser();
	@SuppressWarnings("unused")
	private Level level;

	private boolean tryedArg = false;

	private BufferedImage levelImage;

	private File entitiesFile;

	public int width, height, renderRadius = 10;

	private List<LItemDrop> items = new ArrayList<LItemDrop>();
	private List<LMob> mobs = new ArrayList<LMob>();

	public Level() {
		level = this;
		fileChooser.setDialogTitle(Strings._title);
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	}

	public void add(LMob mob) {
		mobs.add(mob);
	}

	public void add(LItemDrop item) {
		items.add(item);
	}

	public void loadLevel() {
		if (Arguments.getPath() == null) fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		else {
			fileChooser.setCurrentDirectory(new File(Arguments.getPath()));
		}
		int result = JFileChooser.CANCEL_OPTION;
		if (Arguments.getSaveFile() == null) tryedArg = true;
		if (tryedArg == true) result = fileChooser.showOpenDialog(DisplayWindow.getFrame());
		if (result == JFileChooser.APPROVE_OPTION || Arguments.getSaveFile() != null) {
			File path = fileChooser.getSelectedFile();
			if (tryedArg == false && Arguments.getSaveFile() != null) {
				path = new File(Arguments.getPath() + "/" + Arguments.getSaveFile());
				tryedArg = true;
			}
			File levelFile = new File(path.getAbsolutePath() + "/world.json");
			if (levelFile != null) {
				if (levelFile.exists()) {
					levelName = path.getName();
					Log.info("Loading level [" + levelName + "]...");
					try {
						levelImage = Loader.loadBufferedImage(levelFile.toString().replace(".json", ".png"));
						CordPane.levelWidth = width = levelImage.getWidth() / 32;
						CordPane.levelHeight = height = levelImage.getHeight() / 32;
						loadEntityData(path);
					} catch (ParseException | IOException e) {
						Log.warn("Loading level [" + levelName + "]... FAILED");
						Log.stackTrace(e);
						return;
					}
					Log.info("Loading level [" + levelName + "]... SUCCESS");
					loaded = true;
				} else {
					Log.warn("This is not a valid save level");
					Dialogs.error("This is not a valid save level", Strings._title);
					loadLevel();
				}
			}
		}
		if (!loaded) System.exit(0);
	}

	private void loadEntityData(File path) throws ParseException {
		Log.info("Loading entity data");
		entitiesFile = new File(path.getAbsolutePath() + "/entities.json");
		JSONObject entities = (JSONObject) parser.parse(StaticReader.read(entitiesFile.getAbsolutePath().toString(), false));
		{// Mobs
			try {
				JSONArray jsonMobs = (JSONArray) entities.get("mobs");
				for (int m = 0; m < jsonMobs.size(); m++) {
					JSONObject mob = (JSONObject) jsonMobs.get(m);
					int id = Integer.parseInt(mob.get("id").toString());
					int x = Integer.parseInt(mob.get("x").toString());
					int y = Integer.parseInt(mob.get("y").toString());
					List<LItem> loot = new ArrayList<LItem>();
					boolean roam = (boolean) mob.get("roam");
					JSONArray inv = (JSONArray) mob.get("loot");
					for (int i = 0; i < inv.size(); i++) {
						try {
							JSONObject item = (JSONObject) inv.get(i);
							int itemId = Integer.parseInt(item.get("id").toString());
							int itemQu = Integer.parseInt(item.get("no").toString());
							loot.add(new LItem(itemId, itemQu));
						} catch (Exception itemError) {
							Log.warn("Did not load item to mob");
						}
					}
					int dir = Integer.parseInt(mob.get("dir").toString());
					mobs.add(new LMob(id, x, y, roam, dir, loot));
				}
			} catch (Exception mobError) {
				Log.warn("Mob loading error");
				Log.stackTrace(mobError);
			}
		}
		{// Items
			try {
				JSONArray items = (JSONArray) entities.get("items");
				for (int i = 0; i < items.size(); i++) {
					JSONObject entity = (JSONObject) items.get(i);
					int id = Integer.parseInt(entity.get("id").toString());
					int x = Integer.parseInt(entity.get("x").toString());
					int y = Integer.parseInt(entity.get("y").toString());
					int q = Integer.parseInt(entity.get("q").toString());
					this.items.add(new LItemDrop(id, q, new TileCords(x, y)));
				}
			} catch (Exception mobError) {
				Log.warn("Item loading error");
				Log.stackTrace(mobError);
			}
		}
		{// Player
			Log.info("Loading player data");
			try {
				File playerFile = new File(path.getAbsolutePath() + "/player.json");
				JSONObject player = (JSONObject) parser.parse(StaticReader.read(playerFile.getAbsolutePath().toString(), false));
				int x = Integer.parseInt(player.get("x").toString());
				int y = Integer.parseInt(player.get("y").toString());

				List<LItem> loot = new ArrayList<LItem>();
				JSONArray inv = (JSONArray) player.get("loot");
				for (int i = 0; i < inv.size(); i++) {
					try {
						JSONObject item = (JSONObject) inv.get(i);
						int itemId = Integer.parseInt(item.get("id").toString());
						if (itemId >= 0) {
							int itemQu = Integer.parseInt(item.get("no").toString());
							loot.add(new LItem(itemId, itemQu));
						}
					} catch (Exception itemError) {
						Log.warn("Did not load item to player");
					}
				}
				int dir = Integer.parseInt(player.get("dir").toString());
				mobs.add(new LMob(0, x, y, true, dir, loot));
			} catch (Exception playerError) {
				Log.severe("Player loading error");
				Log.stackTrace(java.util.logging.Level.SEVERE, playerError);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void save() {
		JSONObject entities = new JSONObject();
		JSONArray mobs = new JSONArray();
		for (LMob mob : this.mobs) {
			if (mob.getID() != 0) {
				JSONObject obj = new JSONObject();
				obj.put("id", mob.getID());
				obj.put("x", mob.getPosition().getX());
				obj.put("y", mob.getPosition().getY());
				obj.put("dir", mob.getDir());
				obj.put("roam", mob.canRoam());
				obj.put("loot", mob.getLootInJSON());
				mobs.add(obj);
			} else {
				// TODO do correct player save
			}
		}
		entities.put("mobs", mobs);
		JSONArray items = new JSONArray();
		for (LItemDrop item : this.items) {
			JSONObject obj = new JSONObject();
			obj.put("id", item.getId());
			obj.put("x", item.getCords().getX());
			obj.put("y", item.getCords().getY());
			obj.put("q", item.getQuantity());
			items.add(obj);
		}
		entities.put("items", items);
		try {
			FileWriter file = new FileWriter(entitiesFile);
			file.write(entities.toJSONString());
			file.flush();
			file.close();
		} catch (IOException e) {
			Log.severe("Failed to save file");
			Log.stackTrace(e);
		}
	}

	public void render(Graphics g, Point offset, Point render) {
		if (loaded) {
			g.drawImage(levelImage, offset.x - (render.x * 32), offset.y - (render.y * 32), null);
			for (LItemDrop item : items) {
				item.render(g, offset, render);
			}
			for (LMob mob : mobs) {
				mob.render(g, offset, render);
			}
		} else {
			Log.warn("level is not loaded! broken?");
		}
	}

	public LMob getPlayer() {
		for (LMob mob : mobs) {
			if (mob.getID() == 0) return mob;
		}
		return null;
	}
}
