package net.roryclaasen.lost.level;

import net.roryclaasen.lost.graphics.sprite.Sprite;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;

public class LItem {
	protected int id;
	protected int quantity;

	protected Sprite sprite;

	public LItem(int id, int quantity) {
		this.id = id;
		this.quantity = quantity;
		sprite = SpriteSheet.items.get(id);
	}

	public int getId() {
		return id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Sprite getSprite() {
		return sprite;
	}
}
