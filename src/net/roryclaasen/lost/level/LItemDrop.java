package net.roryclaasen.lost.level;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.Colors;
import net.roryclaasen.lost.toolbox.Fonts;
import net.roryclaasen.lost.toolbox.TileCords;

public class LItemDrop extends LItem {

	private TileCords cords;

	public LItemDrop(int id, int quantity, TileCords cords) {
		super(id, quantity);
		this.cords = cords;
	}

	public TileCords getCords() {
		return cords;
	}

	public void render(Graphics g,Point offset, Point render) {
		if (sprite != null) g.drawImage(sprite.getImage(),  offset.x + cords.getX() * 32 - (render.x * 32), offset.y + cords.getY() * 32 - (render.y * 32), null);
		if (quantity > 1) {
			Fonts.drawString(g, quantity, offset.x + cords.getX() * 32 - (render.x * 32), offset.y + cords.getY() * 32 - (render.y * 32), 200);
		}
		if (Arguments.showBounds()) {
			Color c = g.getColor();
			g.setColor(Colors.boxMob);
			g.drawRect(offset.x + cords.getX() * 32 - (render.x * 32), offset.y + cords.getY() * 32 - (render.y * 32), 32, 32);
			g.setColor(c);
		}
	}
}
