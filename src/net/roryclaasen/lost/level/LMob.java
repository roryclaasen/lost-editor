package net.roryclaasen.lost.level;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import net.roryclaasen.lost.graphics.sprite.Sprite;
import net.roryclaasen.lost.graphics.sprite.SpriteSheet;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.Colors;
//import net.roryclaasen.lost.level.entity.MobList;
import net.roryclaasen.lost.toolbox.TileCords;

public class LMob {
	private int id, x, y, dir;

	private boolean roam;

	private List<LItem> loot;

	private Sprite sprite;

	public LMob(int id, int x, int y, boolean roam, int dir, List<LItem> loot) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.roam = roam;
		this.dir = dir;
		this.loot = loot;
		sprite = SpriteSheet.mobs.get(id);
	}

	public void render(Graphics g, Point offset, Point render) {
		if (sprite != null) g.drawImage(sprite.getImage(), offset.x + x * 32 - (render.x * 32), offset.y + y * 32 - (render.y * 32), null);
		if (Arguments.showBounds()) {
			Color c = g.getColor();
			g.setColor(Colors.boxMob);
			g.drawRect(offset.x + x * 32 - (render.x * 32), offset.y + y * 32 - (render.y * 32), 32, 32);
			g.setColor(c);
		}
	}

	public int getID() {
		return id;
	}

	public TileCords getPosition() {
		return new TileCords(x, y);
	}

	public boolean canRoam() {
		return roam;
	}

	public int getDir() {
		return dir;
	}

	public List<LItem> getLoot() {
		return loot;
	}

	@SuppressWarnings("unchecked")
	public JSONArray getLootInJSON() {
		JSONArray loot = new JSONArray();
		for (LItem item : getLoot()) {
			JSONObject obj = new JSONObject();
			obj.put("id", item.getId());
			obj.put("no", item.getQuantity());
			loot.add(obj);
		}
		return loot;
	}
}
