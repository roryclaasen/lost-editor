package net.roryclaasen.lost.toolbox;

import net.gogo98901.log.Log;

public class Arguments {

	private static String path = null;
	private static String saveFile = null;

	public static void check(String[] args) {
		Log.info("Arguments... Checking");
		for (String arg : args) {
			arg = arg.toLowerCase();
			if (arg.startsWith("-")) {
				arg = arg.replaceFirst("-", "");

			}

			{// DEVELOPER ARGUMENTS

			}
			if (arg.startsWith("p=")) path = arg.replace("p=", "");
			if (arg.startsWith("s=")) saveFile = arg.replace("s=", "");
		}
	}

	public static String getPath() {
		return path;
	}

	public static String getSaveFile() {
		return saveFile;
	}

	public static boolean showBounds() {
		return true;
	}
}
