package net.roryclaasen.lost.toolbox;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.io.IOException;

import net.gogo98901.log.Log;
import net.gogo98901.util.Data;
import net.gogo98901.util.Loader;

public class Fonts {
	public static final Font square = loadFont("squarefont/Square.ttf");
	public static final Font squareo = loadFont("squarefont/Squareo.ttf");

	public static Font loadFont(String name) {
		String path = "assets/fonts/" + name;
		try {
			Font font = Font.createFont(Font.TRUETYPE_FONT, Loader.getResourceAsStream(path));
			return font.deriveFont(15F);
		} catch (FontFormatException | IOException e) {
			Log.stackTrace(e);
		}
		Log.warn("Could not load font, using 'Monospaced'");
		return Font.getFont("Monospaced");
	}

	public static void drawString(Graphics g, Object text, int x, int y, int opacity) {
		g.setColor(new Color(Colors.textBack.getRed(), Colors.textBack.getGreen(), Colors.textBack.getBlue(), opacity));
		g.drawString(text.toString(), x + 1, y + 2);
		g.setColor(new Color(Colors.textFront.getRed(), Colors.textFront.getGreen(), Colors.textFront.getBlue(), opacity));
		g.drawString(text.toString(), x, y);
	}

	public static void drawString(Graphics g, Font font, Object text, int x, int y, int opacity) {
		g.setFont(font);
		drawString(g, text, x, y, opacity);
	}

	public static void drawString(Graphics g, Object text, int x, int y) {
		drawString(g, text, x, y, 255);
	}

	public static void drawString(Graphics g, Font font, Object text, int x, int y) {
		drawString(g, font, text, x, y, 255);
	}

	public static void drawCenteredString(Graphics g, Object text, int x, int y, int width, int height, int opacity) {
		Rectangle2D bounds = Data.getStringBounds(text.toString(), g, g.getFont());
		FontMetrics fm = Data.getFontMatrics(text.toString(), g, g.getFont());
		int posX = x + (width - (int) bounds.getWidth()) / 2;
		int posY = y + (height - (int) bounds.getHeight()) / 2 + fm.getAscent();
		drawString(g, text, posX, posY, opacity);
	}

	public static void drawCenteredString(Graphics g, Object text, Font font, int x, int y, int width, int height, int opacity) {
		g.setFont(font);
		drawCenteredString(g, text, x, y, width, height, opacity);
	}

	public static void drawCenteredString(Graphics g, Object text, int x, int y, int width, int height) {
		drawCenteredString(g, text, x, y, width, height, 255);
	}

	public static void drawCenteredString(Graphics g, Object text, Font font, int x, int y, int width, int height) {
		drawCenteredString(g, text, font, x, y, width, height, 255);
	}
}
