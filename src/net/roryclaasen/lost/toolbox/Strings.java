package net.roryclaasen.lost.toolbox;

public class Strings {
	public static final String _title = "Lost Editor";
	public static final String _author = "Rory Claasen";
	public static final String _build = "Alpha";
}
