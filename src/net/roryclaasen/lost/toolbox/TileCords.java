package net.roryclaasen.lost.toolbox;

public class TileCords {
	public static boolean equals(TileCords cords0, TileCords cords1) {
		if ((cords0.getX() == cords1.getX()) && (cords0.getY() == cords1.getY())) return true;
		return false;
	}

	public boolean equals(TileCords cords) {
		return equals(this, cords);
	}

	private int x, y;

	public TileCords(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public String text() {
		return "[x=" + x + ", y=" + y + "]";
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
