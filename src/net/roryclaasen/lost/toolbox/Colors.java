package net.roryclaasen.lost.toolbox;

import java.awt.Color;

public class Colors {
	public static final Color boxTile = Color.BLACK;
	public static final Color boxPlayer = Color.CYAN;
	public static final Color boxMob = Color.PINK;

	public static final Color textBack = Color.BLACK;
	public static final Color textFront = Color.WHITE;
	
	public static final Color selected = Color.YELLOW;

	public static Color setOpacity(Color color, int opacity) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), opacity);
	}
}
