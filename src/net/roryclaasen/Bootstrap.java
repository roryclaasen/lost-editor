package net.roryclaasen;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;

import net.gogo98901.log.Log;
import net.gogo98901.util.Data;
import net.roryclaasen.lost.GameThread;
import net.roryclaasen.lost.toolbox.Arguments;
import net.roryclaasen.lost.toolbox.Strings;
import net.roryclaasen.lost.window.DisplayWindow;

public class Bootstrap {

	public static void main(String[] args) {
		Log.info("Program started");
		Log.info(Strings._title + " by " + Strings._author);
		Log.info("Build: " + Strings._build);
		Log.info("-----------------------------------------------------------------------");
		printSystemData();
		Log.info("-----------------------------------------------------------------------");
		Arguments.check(args);
		if (init()) {
			Log.info("Pre Editor Initialization... OKAY");
			start();
			//Dialogs.infomation("The editor will not edit the map\nUse tiled for that", Strings._title);
		} else {
			Log.severe("Pre Editor Initialization... FAILED");
			Log.severe("Terminated");
		}
	}

	private static boolean init() {
		try {
			Data.setDefultLookAndFeel();
		} catch (Exception e) {
			Log.stackTrace(Level.SEVERE, e);
			return false;
		}
		return true;
	}

	private static void start() {
		Log.info("Starting game");
		if (!System.getProperty("user.name").toLowerCase().equals("travis")) {
			DisplayWindow window = new DisplayWindow();
			window.init();
			GameThread game = new GameThread(window);
			game.init();
			DisplayWindow.setVisible(true);
			game.start();
		} else {
			Log.info("Stopping game");
			Log.info("Travis detected!");
		}
	}

	public static String getSystemData() {
		String text = "";
		try {
			text += "        Workstation: " + InetAddress.getLocalHost().getHostName() + "\n";
		} catch (UnknownHostException e) {
			text += "        Workstation: UNKNOWN\n";
		}
		text += "               Name: " + System.getProperty("user.name") + "\n";
		text += "   Operating System: " + System.getProperty("os.name") + " (" + System.getProperty("os.version") + ")\n";
		text += "System Architecture: " + System.getProperty("os.arch") + "\n";
		text += "        Java Vendor: " + System.getProperty("java.vendor") + "\n";
		text += "       Java Version: " + System.getProperty("java.version") + "\n";
		// text += "     Java VM Vendor: " + System.getProperty("java.vm.specification.vendor") + "\n";
		text += "    Java VM Version: " + System.getProperty("java.vm.specification.version") + "\n";
		text += "    Java Class Path: " + System.getProperty("java.class.path") + "\n";
		text += "  Working Directory: " + System.getProperty("user.dir") + "\n\n";
		text += "              Cores: " + Runtime.getRuntime().availableProcessors() + "\n";
		text += "Free memory (bytes): " + Runtime.getRuntime().freeMemory() + "\n";
		text += "   Free memory (MB): " + (((double) Runtime.getRuntime().freeMemory() / 1024) / 1024) + "\n";
		return text;
	}

	public static void printSystemData() {
		String[] text = getSystemData().split("\n");
		for (String line : text) {
			Log.info("System] " + line);
		}
	}
}
